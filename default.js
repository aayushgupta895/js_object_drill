function defaults(obj, defaultProps) {
    if(obj !== null && typeof obj == 'object' && defaultProps !== null && typeof defaultProps == 'object'){
        for(let key in defaultProps){
              obj[key] = defaultProps[key];
        }
    }else{
        return `passed arguments are not objects`;
    }
  return obj;
}


module.exports = defaults;