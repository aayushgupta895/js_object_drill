
function mapObject(obj, cb) {
    const result = {};
    if(obj != null && typeof obj == 'object'){
        for(let key in obj){
            result[key] = cb(obj[key]);
        }
    }else{
        return `passed argument is not object`;
    }
    return result;
}

module.exports = mapObject;
