function values(obj) {
  const result = [];
  if (obj != null && typeof obj == "object" && Object.values(obj).length > 0) {
    const values = Object.values(obj);
    for (let value of values) {
      if (typeof value == "object") {
        values(value);
      } else {
        result.push(value);
      }
    }
  } else {
    return `passed argument is not object`;
  }
  return result;
}

module.exports = values;
