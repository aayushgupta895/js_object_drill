
function keys(obj) {
    const result = [];
  if (obj != null && typeof obj == "object" && Object.keys(obj).length > 0) {
    const keys = Object.keys(obj);
    for (let key of keys) {
      if (typeof obj[key] == "object") {
        keys(obj[key]);
      } else {
        result.push(key);
      }
    }
  }else{
    return `passed argument is not object`;
  }
  return result;
}


module.exports = keys;